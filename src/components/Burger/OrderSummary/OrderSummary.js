import React from 'react';

import Aux from '../../../hoc/Aux'
import Button from '../../UI/Button/Button' 
import { Component } from 'react';

class OrderSummary extends Component {
    //can also be a functional component

    componentWillUpdate() {
        console.log('[order summary will update]')
    }

    render() {
 
        const ingredientSummary = Object.keys(this.props.ingredients)
        .map(igKey => {
            return <li key={igKey}> <span style={{ textTransform: 'capitalize'}}> {igKey} </span>: {this.props.ingredients[igKey]} </li> 
        });

        return (

            <Aux>
                <h3>My Burger</h3>
                <p>A delicious burger containing the following: </p>
    
                <ul>                
                    {ingredientSummary}
                </ul>
    
                <p> <strong>Total price: {this.props.price}</strong></p>
                <p>Continue to Checkout</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled}> Cancel</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}> Continue</Button>     
    
            </Aux>
        )
    }

}


export default OrderSummary