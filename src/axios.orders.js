import Axios from 'axios'

const instance = Axios.create({
    baseURL: 'https://react-burger-cb521.firebaseio.com'
    // baseURL: 'https://cors-anywhere.herokuapp.com/https://react-burger-cb521.firebaseio.com'

});


export default instance