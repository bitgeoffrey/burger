import React from 'react';

import Layout from './components/Layout/Layout'
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder'
// import { Component } from 'react';


function App() {
  return (
    <div className="App">
      <Layout>
        < BurgerBuilder/>
      </Layout>
    </div>
  );
}

// class App extends Component {
//   state= {
//     show: true
//   }

//   componentDidMount() {
//     setTimeout(()=>{ this.setState({show: false})},3000)
//   }

//   render() {
//     return(
//       <div className="App">
//        <Layout>
//         {this.state.show? < BurgerBuilder/> : null}
//        </Layout>
//      </div>
//     )
//   }
// }


export default App;
