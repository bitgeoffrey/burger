import React, { Component } from 'react';

import Aux from '../../hoc/Aux'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import axios from '../../../src/axios.orders'
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';


const INGREDIENT_PRICES = {
    salad: 20,
    cheese: 15,
    bacon: 30,
    meat: 40
}

class BurgerBuilder extends Component {

    state = {
        // ingredients: {
        //     salad: 0,
        //     cheese: 0,
        //     bacon: 0,
        //     meat: 0
        // },
        ingredients: null,

        totalPrice: 60,
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false
    }

    componentDidMount() {
        // facilitates cleaning up old interceptors
        axios.get('https://react-burger-cb521.firebaseio.com/ingredients.json')
        .then(response => { this.setState({ingredients: response.data})})
        .catch(error => { this.setState({error: error})})
    }

    updatePurchaseState (ingredients) {
        // const ingredients = {...this.state.ingredients}

        const sum = Object.keys(ingredients)
            .map( igKey =>{
                // console.log(ingredients[igKey])
                return ingredients[igKey];
            })
            .reduce((sum, el)=>{
                return sum + el
            }, 0);

            this.setState({ purchasable: sum > 0})
    }

    addIngredientHandler = (type) =>{
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = { ...this.state.ingredients}
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice + priceAddition

        this.setState({ totalPrice: newPrice, ingredients: updatedIngredients});
        this.updatePurchaseState(updatedIngredients)

    }

    removeIngredientHandler = (type) =>{
        const oldCount = this.state.ingredients[type];
        if (oldCount <= 0) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredients = { ...this.state.ingredients}
        updatedIngredients[type] = updatedCount;
        const priceDeduction = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice - priceDeduction

        this.setState({ totalPrice: newPrice, ingredients: updatedIngredients});
        this.updatePurchaseState(updatedIngredients)
    }

    purchaseHandler =() => {
        this.setState({ purchasing: true});
    }

    purchaseCancelHandler =() => {
        this.setState({ purchasing: false});
    }

    purchaseContinueHandler =() => {
        // alert('You continue');
        this.setState({ loading: true})

        const order = {
            ingredients: this.state.ingredients,
            price: this.state.totalPrice,
            customer: {
                name: 'Sammy Mungai',
                address: 'Ktown',
            },
            delivery: 'chopper'
        }
        axios.post('/orders.json', order)
        .then(response =>  {
            this.setState({ loading: false});
        console.log(response)} )
        .catch(error => {
            this.setState({ loading: false});
            console.log(error)
        })
    }

    render() {
        const disabledInfo = {...this.state.ingredients}

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
            // console.log(disabledInfo[key])
        }

        let orderSummary = null;

         let burger = this.state.error? <p>Sorry ingredients could not be loaded </p> : <Spinner/>;

         if (this.state.ingredients) {
             burger = (
                <Aux>
                    <Burger ingredients={this.state.ingredients} />
                    <BuildControls
                    ingredientAdded = {this.addIngredientHandler}
                    ingredientRemoved = {this.removeIngredientHandler}
                    disabled={disabledInfo}
                    price={this.state.totalPrice}
                    ordered={this.purchaseHandler}
                    purchasable={this.state.purchasable}/>
                </Aux>
             );

             orderSummary = <OrderSummary ingredients={this.state.ingredients}
        price= {this.state.totalPrice.toFixed(2)}
         purchaseCancelled={this.purchaseCancelHandler}
         purchaseContinued={this.purchaseContinueHandler} />
             
         }

         if(this.state.loading) {
            orderSummary = <Spinner/>
         }

        return(
            <Aux>
                <Modal show={this.state.purchasing}  modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>

                {burger}
                
            </Aux>   
        );
    }
}

export default withErrorHandler(BurgerBuilder, axios)